<!--
SPDX-FileCopyrightText: 2020 Intevation GmbH

SPDX-License-Identifier: MIT
-->

# Mailman3 - GPG WKD support for service mails

Sign and encrypt service mails with gpg wkd.

## Prerequisities

* mailman3
* python3-gpg, gpgme 1.12.0

## Installation

1. Add following lines to mailman/config/schema.cfg at the bottom of the file:
[gpg]
gnupghome:
signkey:
2. Add line to mailman/pipelines/virgin.py
     _default_handlers = (
        'cook-headers',
        'rfc-2369',
        'signencrypt', ## this is the new line
        'to-outgoing',
        )
3. Copy handlers/signencrypt.py to mailman/handlers/
    cp handlers/signencrypt.py <path to mailman>/handlers/

## Configuration
1. Create a gnupghome. e.g. /tmp/dot.gnupg
2. Create a signkey without password in this gnupghome. See gpg2.
3. Configure the mailman.cfg:
   [gpg]
   gnupghome: <path to gnupghome>
   signkey: <signkey uid>

## Limitations
The mechanism uses wkd (web keys directory) to get the public key for
the encryption. The code is experimental. It is just a proof of concept.

## Licence
MIT License Copyright (c) 2020 Intevation GmbH

